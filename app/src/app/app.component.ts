import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideMenuComponent } from 'side-menu/src/public-api';
import { Coding } from '@his-base/datatypes/dist/datatypes';
import { MenuTitle } from 'side-menu/src/lib/side-menu.interface';
import { RouterModule } from '@angular/router';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, SideMenuComponent,RouterModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  handleMenuItemCodeClick(code: string) {
    console.log('MenuItem code clicked:', code);
  }

  menuTitle: MenuTitle[] = [
    {
      icon: 'pi pi-book',
      title: new Coding({ code: "diagnosisIssued", display: "診斷開立"}),
      child: [
        new MenuTitle({ title: new Coding({ code: "physicianNotes", display: "醫師備註"})}),
        new MenuTitle({ title: new Coding({ code: "nurseRemarks", display: "護理師備註"})}),
      ]
    },
    {
      icon: 'pi pi-list',
      title: new Coding({ code: "prescription", display: "開立處方"}),
      child: [
        new MenuTitle({title: new Coding({ code: "inspection", display: "檢驗檢查項目"})}),
        new MenuTitle({title: new Coding({ code: "radiation", display: "放射檢查"})}),
        new MenuTitle({title: new Coding({ code: "internalMedicine", display: "內科檢查"})}),
        new MenuTitle({title: new Coding({ code: "surgical", display: "外科檢查"})}),
        new MenuTitle({title: new Coding({ code: "obstetrics", display: "婦產檢查"})}),
        new MenuTitle({title: new Coding({ code: "child", display: "兒童檢查"})}),
        new MenuTitle({title: new Coding({ code: "other", display: "其他檢查"})}),
        new MenuTitle({title: new Coding({ code: "treat", display: "治療及處置"})}),
        new MenuTitle({title: new Coding({ code: "medication", display: "院內用藥"})}),
        new MenuTitle({title: new Coding({ code: "own", display: "自備用藥"})}),
      ]
    }
  ]

}

