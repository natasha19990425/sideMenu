import { provideAnimations } from '@angular/platform-browser/animations';
import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { AppComponent } from './app.component';

export const appConfig: ApplicationConfig = {
  providers: [provideAnimations(),provideRouter([
    {
      path:"**",
      component: AppComponent,
    }
  ])]
};
