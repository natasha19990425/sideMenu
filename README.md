# SideMenu (Component)

## 專案說明
- 開發環境為 Angular CLI version 16.1.8 / Node v18.16.1
- 以 Angular CLI Library 建立專案。
- 需由外部傳送資料，才有有資料呈現在畫面上。 

## 啟動專案
- 安裝相依套件: `npm install`
- 啟動專案: `npm start`
- 啟動 ESlint 格式檢測: `npm lint`
- 產生 compodoc 網頁文件: `npm compodoc:build`
- 啟動 compodoc 網頁文件: `npm compodoc:serve`


## 使用專案
**載入套件**
* 將 gitlab 上 clone 網址放入 package.json 的 dependencies 中，輸入 `npm install` 指令進行下載

- 要記得更改 package.json 預設 port 值為8081 `compodoc:serve": "compodoc -s --port 8081`
避免與後端port重複產生問題
