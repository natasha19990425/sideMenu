import { Coding } from "@his-base/datatypes/dist/datatypes";

export class MenuTitle{
  title: Coding = new Coding();
  child: MenuTitle[] = [];
  icon = '';

  constructor(that? :Partial<MenuTitle>){
    Object.assign(this, structuredClone(that))
  }
}

