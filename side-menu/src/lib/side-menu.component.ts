import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ButtonModule } from 'primeng/button';
import { MenuItem } from 'primeng/api';
import { MenuTitle } from './side-menu.interface';
import { RouterModule } from '@angular/router';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'his-side-menu',
  standalone: true,
  imports: [CommonModule, PanelMenuModule, ButtonModule,RouterModule],
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent {

  #menuTitle: MenuTitle[] = [];
  items: MenuItem[] = [];
  @Input({ required: true }) set menuTitle(menuTitle: MenuTitle[]) {
    this.#menuTitle = menuTitle;
    this.items =  this.#menuTitle.map(v =>{
      return { icon: v.icon, label: v.title.display, items: v.child.map(v => {
        return { label: v.title.display ,command: this.onMenuItemClick(v) }}) } as MenuItem });
  };

  @Output() menuItemCode: EventEmitter<string> = new EventEmitter<string>();


  onMenuItemClick(menuTitle: MenuTitle) {

    return menuTitle.child.length?()=>{return}:()=>{this.menuItemCode.emit(menuTitle.title.code)}
  }
}

